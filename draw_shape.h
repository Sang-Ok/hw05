// draw_shape.h

#ifndef _DRAW_SHAPE_H_
#define _DRAW_SHAPE_H_

#include <iostream>
#include <vector>
#include <string>
enum { RECTANGLE, TRIANGLE_UP, TRIANGLE_DOWN };
enum { ERROR_OUT_OF_CANVAS = -1, ERROR_INVALID_INPUT = -2 };

struct Shape {
  int type;
  int x, y;
  int width, height;
  char brush;  // The character to draw the shape.
};

class Canvas {
 public:
  Canvas(size_t row, size_t col){width_=row, height_=col;}
  ~Canvas(){}

  int width() const{return width_;}
  int height()const{return height_;}
  int AddShape(const Shape &s);  // Return the index of the shape.
  bool Out(int x,int y);
  void DeleteShape(int& index){shapes_.erase(shapes_.begin()+index);}
  void DrawOne(std::vector<std::string>& map, Shape s)const;
  void Draw(std::ostream& os)const;
  void Dump(std::ostream& os)const;

 private:
  size_t width_, height_; 
  std::vector<Shape> shapes_;
};

#endif  // _DRAW_SHAPE_H_
