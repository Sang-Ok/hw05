// sorted_array.h

#ifndef _SORTED_ARRAY_H_
#define _SORTED_ARRAY_H_

#include <vector>
#include <algorithm>
using namespace std;
#define TEST	cout<<'\t'<<
static bool small(int i, int j){ return(i>j);}

class SortedArray {
 public:
  SortedArray(){}
  ~SortedArray(){}

  void AddNumber(int num){numbers_.push_back(num);}

  vector<int> GetSortedAscending() const{
    vector<int>ret=numbers_;
    sort(ret.begin(),ret.end());
    return ret;}
  vector<int> GetSortedDescending() const{
    vector<int>ret=numbers_;
    sort(ret.begin(),ret.end(), small);
    return ret;}
  int GetMax() const{	vector<int>::const_iterator it;   int max=numbers_[0];
    for(it=numbers_.begin();it!=numbers_.end();it++)      if(max<*it)	max=*it;
      return max;
  }
  int GetMin() const{	vector<int>::const_iterator it;   int i,min=numbers_[0];
    for(it=numbers_.begin();it!=numbers_.end();it++)      if(min>*it)	min=*it;
      return min;
}

 private:
  vector<int> numbers_;
};

#endif  // _SORTED_ARRAY_H_
