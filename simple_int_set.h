// simple_int_set.h

#ifndef _SIMPLE_INT_SET_H_
#define _SIMPLE_INT_SET_H_

#include <iostream>
#include <set>

using namespace std;

set<int> SetIntersection(const set<int>& set0, const set<int>& set1);
set<int> SetUnion(const set<int>& set0, const set<int>& set1);
set<int> SetDifference(const set<int>& set0, const set<int>& set1);

bool InputSet(istream& in, set<int>* s);
void OutputSet(ostream& out, const set<int>& s);

#endif  // _SIMPLE_INT_SET_H_
