// draw_shape_main.cc

#include <iostream>
#include <string>
#include "draw_shape.cc"

using namespace std;

int main() {
  size_t row, col;
  cin >> row >> col;
  Canvas canvas(row, col);
  canvas.Draw(cout);
  while (true) {
    string tok;
    cin >> tok;
    if (tok == "add") {
      string type;
      cin >> type;
      Shape shape;
      if (type == "rect") {
        shape.type = RECTANGLE;
        cin >> shape.x >> shape.y >> shape.width >> shape.height >> shape.brush;
      } else if (type == "tri_up" || type == "tri_down") {
        shape.type = type == "tri_up" ? TRIANGLE_UP : TRIANGLE_DOWN;
        cin >> shape.x >> shape.y >> shape.height >> shape.brush;
        shape.width = 2 * shape.height - 1;
      } else {
        continue;
      }
      switch (canvas.AddShape(shape)) {
        case ERROR_OUT_OF_CANVAS:
          cout << "error out of canvas" << endl;
          break;
        case ERROR_INVALID_INPUT:
          cout << "error invalid input" << endl;
          break;
      }
    } else if (tok == "delete") {
      int index;
      cin >> index;
      canvas.DeleteShape(index);
    } else if (tok == "draw") {
      canvas.Draw(cout);
    } else if (tok == "dump") {
      canvas.Dump(cout);
    } else {
      break;
    }
  }
  return 0;
}
