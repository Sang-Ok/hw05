// simple_int_set_main.cc

#include <iostream>
#include <string>
#include <vector>
#include "simple_int_set.cc"

using namespace std;

int main() {
  while (true) {
    set<int> set0, set1;
    if (InputSet(cin, &set0) == false) break;
    string op;
    cin >> op;
    if (op != "+" && op != "-" && op != "*") break;
    if (InputSet(cin, &set1) == false) break;
    set<int> res(op == "+" ? SetUnion(set0, set1) :
                 op == "-" ? SetDifference(set0, set1) :
                             SetIntersection(set0, set1));
    OutputSet(cout, res);
  }
  return 0;
}

