// draw_shape.cc
#include "draw_shape.h"
#define TEST	std::cout<<"\t\tTEST::"<<

void Canvas::DrawOne(std::vector<std::string>& map, Shape s)const{
	int i,j,count=0;
	if(s.type==RECTANGLE){		//TEST" SHAPE IS ::  RECTANGLE"<<std::endl;
		for(j = s.y - s.width/2; j <= s.y + s.width/2; j++)
			for(i = s.x - s.height/2; i <= s.x + s.height/2; i++)
				map[j][i] = s.brush;
	}
	else if(s.type==TRIANGLE_DOWN){	//TEST" SHAPE IS ::  TRI_DOWN	HEIGHT::"<<s.height<<std::endl;
		const int& h= s.height-1;
		for(j = s.y; j >= s.y - h; j--, count++)
			for(i=s.x- count; i <= s.x+count;  i++)
				map[j][i] = s.brush;
	}
	else if (s.type==TRIANGLE_UP){	//TEST" SHAPE IS ::  TRI_UP	HEIGHT::"<<s.height<<"	TRACKING START"<<std::endl;
		const int& h= s.height-1;
		for(j = s.y; j <= s.y + h; j++, count++)
			for(i=s.x- count; i <= s.x+count;  i++){
				map[j][i] = s.brush;			//TEST"map["<<j<<"]["<<i<<"] = "<<s.brush<<std::endl;
			}
	}
}

void Canvas::Draw(std::ostream& os)const{
	int x,y;
	std::string str;
	std::vector<std::string> map;		//TEST"GET INTO Draw(os)"<<std::endl;
	for(x=0;x<width_;x++)
		str+='.';

	for(y=0;y<height_;y++){
		map.push_back(str);
	}					//TEST"PRE-CANVAS MAKING FIN	GET INTO SHAPE DRAWING"<<std::endl;

	std::vector<Shape>::const_iterator cit;
	for(cit=shapes_.begin(); cit != shapes_.end(); cit++){
		DrawOne(map, *cit);		//TEST"END DrawOne f(x)s"<<std::endl;
	}					
	std::cout<<' ';
	for(x=0;x<width_;x++)
		std::cout<<x;
	std::cout<<std::endl;
	for(y=0;y<height_;y++){
		std::cout<<y<<map[y]<<std::endl;
	}
}

void Canvas::Dump(std::ostream& os)const{
	std::string str;
	std::vector<Shape>::const_iterator cit = shapes_.begin();
	int i;
	for(i=0 ;   cit+i != shapes_.end() ;  i++){
		const Shape&  s= shapes_[i];
		str=	s.type==RECTANGLE	?	"rect"	:
			s.type==TRIANGLE_UP	?	"tri_up":
							"tri_down";
		if(str=="rect")
			std::cout<<i<<' '<<str<<' '<<s.x<<' '<<s.y<<' '<<s.width<<' '<<s.height<<' '<<s.brush<<std::endl;
		else
			std::cout<<i<<' '<<str<<' '<<s.x<<' '<<s.y<</* <<s.width*/' '<<s.height<<' '<<s.brush<<std::endl;
	}
}

bool Canvas::Out(int x,int y){
/*TEST*/if(0 <=x && 0 <= y && x < width_ && y < height_){
		//TEST"OK:: ("<<x<<", "<<y<<")\tWIDTH&SIZE IS:: ("<<width_<<", "<<height_<<')'<<std::endl;
	}else	//TEST"NO:: ("<<x<<", "<<y<<")\tWIDTH&SIZE IS:: ("<<width_<<", "<<height_<<")\t\t IT RETURNS true FOR Out()"<<std::endl;
	return !(0 <=x && 0 <= y && x < width_ && y < height_);
}

int Canvas::AddShape(const Shape &s){
	//TEST"GET IN TO AddShape()"<<std::endl;
	if(s.type==RECTANGLE){
		if(s.width%2==0 || s.height%2==0)
			return ERROR_INVALID_INPUT;
		else if(Out(s.x + s.width/2,s.y + s.height/2) || Out(s.x - s.width/2,s.y + s.height/2) || Out(s.x + s.width/2,s.y - s.height/2) || Out(s.x - s.width/2,s.y - s.height/2) )
			return ERROR_OUT_OF_CANVAS;
	}
	else{								//TEST"IT'S NOT RECT"<<std::endl;
		int way= (s.type==TRIANGLE_DOWN? +1:-1);
		if(Out(s.x + s.width/2, s.y) || Out(s.x - s.width/2, s.y) || Out(s.x, s.y + way* s.height/2) )
			return ERROR_OUT_OF_CANVAS;
	}
	//TEST"PRE-CHECK SUCCESS, GET INTO ADDING TO shapes_ VECTOR"<<std::endl;
	shapes_.push_back(s);
	return s.type;			  // Return the index of the shape.
}




//		//TEST""<<std::endl;













