// message_book_main.cpp

#include <iostream>
#include <string>
#include <vector>
#include "message_book.cc"

using namespace std;

int main() {
  MessageBook book;
  while (true) {
    string cmd;
    cin >> cmd;
    if (cmd == "add") {
      int number;
      string message;
      cin >> number;
      getline(cin, message);
      book.AddMessage(number, message);
    } else if (cmd == "delete") {
      int number;
      cin >> number;
      book.DeleteMessage(number);
    } else if (cmd == "print") {
      int number;
      cin >> number;
      cout << book.GetMessage(number) << endl;
    } else if (cmd == "list") {
      vector<int> numbers = book.GetNumbers();
      for (int i = 0; i < numbers.size(); ++i) {
        cout << numbers[i] << ": " << book.GetMessage(numbers[i]) << endl;
      }
    } else {
      break;
    }
  }
  return 0;
}

