// simple_int_set.cc
#ifndef _SIMPLE_INT_SET_CC_
#define _SIMPLE_INT_SET_CC_
#include "simple_int_set.h"
#include <iostream>
#include <set>
#include <stdlib.h>
#include <string>

#define TEST	cout<<'\t'<<

inline bool IsInt(const string& str) {
  bool ret = false;
  for (int i = 0; ret == false && i < str.size(); ++i) {
    ret = ('0' <= str[i] && str[i] <= '9');
  }
  return ret;
}


using namespace std;
set<int> SetIntersection(const set<int>& set0, const set<int>& set1){
	set<int> ret;
	for(set<int>::iterator it=set0.begin();  it!=set0.end();  it++)
		if(set1.end()==set1.find(*it))
			continue;
		else
			ret.insert(*it);
	return ret;
}
set<int> SetUnion(const set<int>& set0, const set<int>& set1){
	set<int> ret;
	set<int>::iterator it;
	for(it=set0.begin();  it!=set0.end();  it++)
		ret.insert(*it);
	for(it=set1.begin();  it!=set1.end();  it++)
		ret.insert(*it);

	return ret;
}
set<int> SetDifference(const set<int>& set0, const set<int>& set1){
	set<int> ret;
	for(set<int>::iterator it=set0.begin();  it!=set0.end();  it++)
		if(set1.end()==set1.find(*it))
			ret.insert(*it);
		else
			continue;
	return ret;
}

bool InputSet(istream& in, set<int>* s){
	string str;
	cin>>str;
	s->clear();
	//TEST"START SET f(x)"<<endl;
	if(str != "{")	return false;
	while(cin>>str){
		if(IsInt(str))		s->insert(atoi(str.c_str()));
		else if(str=="}"){
			//TEST"CASE2"<<endl;
			return true;
		}
		else{
			//TEST"CASE3"<<endl;
			return false;
		}
	}
	//TEST"FINISHING SET f(x)"<<endl;
}
void OutputSet(ostream& out, const set<int>& s){
	cout << "{";
	for (set<int>::iterator it=s.begin(); it != s.end(); it++) 
		cout << " " << *it;
	cout << " }" << std::endl;
}


#endif	// simple_int_set.cc
