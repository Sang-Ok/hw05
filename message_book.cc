// message_book.cc
#include "message_book.h"
#ifndef _MESSAGE_BOOK_CC_
#define _MESSAGE_BOOK_CC_

void MessageBook::AddMessage(int number, const std::string& message){
  messages_[number]=message;//main함수의 getline()에서 ' '하나를 앞에 기본적으로 받음. 또한 list할때는 출력할때 한칸 더 출력함. 따라서 메인함수의 오류를 인위적으로 잡지않는 이상 과제조건에 나온대로의 프로그래밍이 불가능.
}
void MessageBook::DeleteMessage(int number){
  messages_.erase(messages_.find(number));
}
std::vector<int> MessageBook::GetNumbers() const{
  std::vector<int> ret;
  std::map<int,std::string>::const_iterator it;
  for(it=messages_.begin();  it!=messages_.end();  it++)
    ret.push_back(it->first);
  return ret;
}
const std::string& MessageBook::GetMessage(int number) const{
  std::map<int, std::string>::const_iterator	it=messages_.find(number);
  if( messages_.end() != it )
    return it->second;
  else
    return "";
}

#endif
